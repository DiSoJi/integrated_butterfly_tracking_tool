# -*- coding: utf-8 -*-
"""
Butterflies application - SIP-Lab.

AppCore.py
Created: 2018/08/10
Author: Abraham Arias Chinchilla

This module performs the task of controlling all the algorithms,
for this version the algorithms are:
    Rotation
    Intrinsic Calibration
    Extrinsic Calibration
Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    A (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

Todo:
    * A
    * B

"""

import cv2
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtCore import Qt
from PyQt5.QtCore import QObject
import time
import numpy as np
from .utils.Settings import Settings
from .Algorithms.CalibrationAlgorithm import CalibrationAlgorithm
from .Algorithms.RotationAlgorithm import RotationAlgorithm

import PySpin


class AppCore(QObject):
    """This .

    The __init__ method may be documented in either the class level
    docstring, or as a docstring on the __init__ method itself.

    Either form is acceptable, but the two should not be mixed. Choose one
    convention to document the __init__ method and be consistent with it.

    Note:
        Do not include the `self` parameter in the ``Args`` section.

    Args:
        msg (str): Human readable string describing the exception.
        code (:obj:`int`, optional): Error code.

    Attributes:
        msg (str): Human readable string describing the exception.
        code (int): Exception error code.

    """

    def __init__(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        super(AppCore, self).__init__()
        self.settings = Settings()
        self.camerasList = [-1, -1, -1]  # Cam0, Cam1, Cam2
        self.Calib_Alg = CalibrationAlgorithm(self.settings)
        self.Rot_Alg = RotationAlgorithm(self.settings)
        self.calibrationDone = False

        self.imgSmallSize = (350, 200)
        self.imgLargeSize = (410, 270)

        #INIT CAMERAS FLIR
        self.systemFLIR = PySpin.System.GetInstance()
        self.cam_list = self.systemFLIR.GetCameras() #This finds how many FLIR cameras are conected

    def openCameras(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        nCam = 0
        #MEMORANDUM: Change 2 for 3
        for i in range(0, 3): #TODO: This value has to be the same that the amount of FLIR cámeras conected (when we are using FLIR cameras)
            cam = self.openCamera(i)
            self.camerasList[i] = cam
            #if cam != -1:
            nCam += 1
        print("Finished starting cameras")
        return nCam


    def openCamera(self, id):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.
            :param id:

        """

        if self.settings.cam_vec_type[id]==0:
            print("trying to init USB cameras")
            print(id)
            #TODO: This is because my PC is nuts, change if your PC behaves like a normal and sane piece of hardware
            if id==1:
                id= 4#Has to be the video id of the device that handles the camera. /dev/video(id)
            cam = cv2.VideoCapture(id)
            if cam.isOpened():
                return cam
            else:
                return -1
        elif self.settings.cam_vec_type[id]==1:
            id -= 1
            print("trying to init FLIR cameras")
            print(id)
            if len(self.cam_list)>0:
                if id != len(self.cam_list):
                    self.cam_list[id].Init()
                    return self.cam_list[id]
                else:
                    return -1
            else:
                return -1
        else:
            print("Unknown type of camera protocol: "+str(id)+". AppCore:openCamera()")

    def CamerasAvailable(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        if self.camerasList[0] == -1 or self.camerasList[1] == -1:
            return False
        else:
            return True

    def closeCameras(self):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        for i in range(0, len(self.camerasList)): #TODO: COMPLETED: Erased a -1 that prevented the cam at position 2 to be close. range ignores the last value so the subtraction made it to be 0 and 1 only
            if self.camerasList[i] != -1:
                if self.settings.cam_vec_type[i]==1:
                    print("Deinitializing FLIR Cameras")
                    self.camerasList[i].DeInit()
                else:
                    self.camerasList[i].release()


    def convertCVtoQT(self, image, scaleSize):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.
            :param image:
            :param scaleSize:

        """
        qimg = QImage(image.data,
                      image.shape[1],
                      image.shape[0],
                      QImage.Format_RGB888)
        qpmap = QPixmap.fromImage(qimg.scaled(scaleSize[0],
                                              scaleSize[1],
                                              Qt.KeepAspectRatio))
        return qpmap

    def captureImage(self, cameraID):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.
            :param cameraID:

        """
        if self.camerasList[cameraID] is None:# or self.camerasList[cameraID]== -1:
            image = cv2.imread('icons/camera_not_available.png',
                               cv2.IMREAD_COLOR)
            return image
        else:
            #For a normal camera
            if self.settings.cam_vec_type[cameraID]==0:
                # READ CAMERA
                #print("it ain't much")

                #print(cameraID)
                try:
                    ret, frame = self.camerasList[cameraID].read()
                    if ret:
                        rgbImage = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    else:
                        rgbImage = np.zeros((512, 512),dtype=int) #TODO:Type data not understood. COMPLETED: took out unecesary argument "3" to set default for C. And added dtype=int to format the data, the lack of this was causing the issue
                except AttributeError:
                    print("AttributeError: Camera with ID: ",cameraID, " is not connected")
                #print(rgbImage.shape)
                try:
                    return rgbImage
                except UnboundLocalError:
                    print("Camera with ID: ",cameraID, "not connected")
            #For a FLIR camera
            else:
                #print("But it's honest work")
                cam = self.camerasList[cameraID]  # self.cam_list[0]
                if isinstance(cam,int):
                    final = np.zeros((512, 512), dtype=int)
                else:
                    #print("1 2 3 testing")
                    cam.BeginAcquisition()
                    img = cam.GetNextImage()

                    img2 = img.Convert(PySpin.PixelFormat_Mono8, PySpin.NO_COLOR_PROCESSING)
                    # i = np.asarray(img2.GetData())
                    order = img2.GetData().reshape((1024, 1280))
                    final = cv2.cvtColor(order, cv2.COLOR_GRAY2RGB)

                    cam.EndAcquisition()
                return final

    def captureAllImages(self, selectedCamera, runningCalibRotationFlag, showExtCalib):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.
            :param selectedCamera:
            :param runningCalibRotationFlag:
            :param showExtCalib:

        """
        img0 = self.captureImage(0)
        img1 = self.captureImage(1)
        img2 = self.captureImage(2)
        if selectedCamera == 2:
            imgToDraw = img2
        elif selectedCamera == 1:
            imgToDraw = img1
        else:
            imgToDraw = img0
        imgs_originals = [img0, img1, img2]
        try:
            images = [img0.copy(),
                      img1.copy(),
                      img2.copy(),
                      imgs_originals[selectedCamera].copy()]
        except AttributeError:
            print("Camera not connected, can't take pictures")

        # Check if camera is available
        inst = 0
        error = 0
        if self.CamerasAvailable:
            try:
                images, patternsFound, inst, error = self.drawInstructions(
                    images,
                    selectedCamera,
                    runningCalibRotationFlag,
                    showExtCalib)
            except UnboundLocalError:
                print("Camera not connected no pictures aviable")
        else:
            print("Camera not available")
        try:
            imgWithInst = [self.convertCVtoQT(images[0], self.imgSmallSize),
                           self.convertCVtoQT(images[1], self.imgSmallSize),
                           self.convertCVtoQT(images[2], self.imgSmallSize),
                           self.convertCVtoQT(images[3], self.imgLargeSize)]
            return [imgs_originals, imgWithInst, patternsFound, inst, error]
        except UnboundLocalError:
            print("Camera not connected, images not aviable")
    def drawInstructions(
            self,
            imgs,
            cameraIDselected,
            runningCalibRotationFlag,
            showExtCalib):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.
            :param imgs:
            :param cameraIDselected:
            :param runningCalibRotationFlag:
            :param showExtCalib:

        """
        patternsFound = 0
        inst = 0
        error = 0
        if runningCalibRotationFlag == "ROTATION":
            # Find and draw markers
            imgs[cameraIDselected], inst = self.Rot_Alg.drawMarkersInstructions(
                imgs[cameraIDselected],
                cameraIDselected)
        elif runningCalibRotationFlag == "DISTORTION":
            # Search the pattern in the selected camera
            patternsFound = self.Calib_Alg.findChessboardsFast(imgs)
            # Only if the pattern is in more than one input image
            try:
                if sum(patternsFound) >= 1:
                    imgs, error = self.Calib_Alg.drawChessboard(
                        imgs,
                        patternsFound,
                        cameraIDselected,
                        self.calibrationDone,
                        showExtCalib)
            except:
                pass

        return [imgs, patternsFound, inst, error]

    def SaveCalibrationImages(self, imgs, cameraIDSelected, calibrationType):
        """To comment.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.
            :param imgs:
            :param cameraIDSelected:
            :param calibrationType:

        """ 
        boolDone = False
        if calibrationType == 0:
            nStart = cameraIDSelected
            nImages = cameraIDSelected+1
        else:
            nStart = 0
            nImages = 3

        for i in range(nStart, nImages):
            boolDone = self.settings.saveCalibrationImages(
                i, imgs[i], calibrationType)

        return boolDone

    def getAmountOfCalibrationWorkDoneForCamera(self):
        """To do.

        A

        Args:
            A (int): A.

        Returns:
            bool: R.

        """
        work = self.settings.getPercentageCalibImagesCaptured()
        return work

    #def runCalibrationAlg(self, progress_callback,calName):
    def runCalibrationAlg(self, progress_callback):
        """Use this function executes the calibration algorithm.

        The cali_alg object wraps the implementations:

        Args:
            progress_callback (*): callback to emit from the thread.

        Returns:
            bool: The result of the algorithm execution.

        """
        self.calibrationDone = True

        #return self.Calib_Alg.Calibration(progress_callback,calName)
        return self.Calib_Alg.Calibration(progress_callback)
