import cv2
import numpy as np

def createObjPoints(patternSize, height, witdh):
    '''
    Points representing the plane
    with points,
    (0,0,0), (0,1,0), (0,2,0), (0,3,0) ... (1,1,0)

    patternSize: tuple (x,y). x*y=number of points
    squareSize: size in cm, mm or even m of a square size
    '''
    objp = np.zeros( (patternSize[0] * patternSize[1],3), np.float32)
    index = 0

    for i in range(0, patternSize[1]): # Witdh
        for j in range(0, patternSize[0]): # Height
            objp[index] = [ j*height, i*witdh, 0 ]
            index +=1
    #objp[:,:2] = np.mgrid[0:patternSize[0],0:patternSize[1]].T.reshape(-1,2)
    #print(objp)
    return objp
#objp = createObjPoints( (7,6), 1.2 )


def map(P, R, T):
    '''
    Maps points according to the 
    rotation matrix and translation vector 
    In the form:
        P' = (P * R) + T
    '''

    P2 = np.zeros( (len(P),3) )
    for i in range(0, len(P)):
        val = np.dot( R, np.transpose(P[i]) ) + np.transpose(T)
        P2[i] = val
    return P2

def drawAxis(img, imgpts):
    '''
    Requieres 4 points 
    in (x,y) form, to draw an axis in the input image
    '''
    center = tuple(imgpts[0].ravel())
    try:
        img = cv2.line(img, center, tuple(imgpts[1].ravel()), (255,0,0), 5)
        img = cv2.line(img, center, tuple(imgpts[2].ravel()), (0,255,0), 5)
        img = cv2.line(img, center, tuple(imgpts[3].ravel()), (0,0,255), 5)
    except OverflowError:
        print("OverflowError: signed integer is greater than maximum")


def promediateCols(mtx):
    '''
    Ignore nan values for the operation
    '''
    means = np.zeros( (3,3) )
    try:
        means = np.nanmean(mtx, axis=1)
    except RuntimeWarning:
        pass

    return means

def calcError(imgpoints, objpoints, rvecs, tvecs, mtx, dist):
    mean_error = 0
    for i in range(0,len(objpoints)):
        imgpoints2, _ = cv2.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
        error = cv2.norm(imgpoints[i],imgpoints2, cv2.NORM_L2) / len(imgpoints2)
        mean_error += error

    return mean_error/len(objpoints)

'''
def getClosestToMean(mean, V):
    
    return []

array = np.matrix([[1,1,1],
                   [1,1,1]])

print(array)
pro = promediateCols(array)

print(pro)
'''