#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  test.py
#
#  Copyright 2019 Geova <geova@geova-X555LA>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
from tkinter import *
import tkinter as tk
import sys
from readXML import readXml
import numpy as np
import cv2
from PIL import Image, ImageTk
import os
from functools import partial
import math
import time
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from mpl_toolkits.mplot3d import Axes3D

# Projection's matrix
K1, K2 = np.zeros((3, 4), np.float32), np.zeros((3, 4), np.float32)

ix, iy, fx, fy = -1, -1, -1, -1
stdR, stdG, stdB = 1, 1, 1
meanR, meanG, meanB = 0, 0, 0
drawing = False
ready = False
closeAll = False
createPlot = False

lower0 = np.zeros((1, 3), np.float32)
upper0 = np.zeros((1, 3), np.float32)
lower1 = np.zeros((1, 3), np.float32)
upper1 = np.zeros((1, 3), np.float32)

##############
global last_frame  # creating global variable
#last_frame = np.zeros((240, 360, 3), dtype=np.uint8)
last_frame = np.zeros((480, 640, 3), dtype=np.uint8)
global cap, flag, frame, n, cap1
cap0 = cv2.VideoCapture(0)
cap1 = cv2.VideoCapture(2)
setCamID = -1
trackBall = False
plotted = False
global lmain, lmain2, lmain3
global pic
global frame
global v
global w
alfa = 1.5


##########
# Borrar Esto
class KalmanFilter:
    kf = cv2.KalmanFilter(4, 2)
    kf.measurementMatrix = np.array([[1, 0, 0, 0], [0, 1, 0, 0]], np.float32)
    kf.transitionMatrix = np.array([[1, 0, 1, 0], [0, 1, 0, 1], [0, 0, 1, 0], [0, 0, 0, 1]], np.float32)

    def Estimate(self, coordX, coordY):
        ''' This function estimates the position of the object'''
        measured = np.array([[np.float32(coordX)], [np.float32(coordY)]])
        self.kf.correct(measured)
        predicted = self.kf.predict()
        return predicted


def clip(x):
    aux = int(np.min([np.max([0, x]), 255]))
    return aux


def showProjectionMatrix():
    K1, K2 = readXml()
    print("Projection matrix K1")
    print(K1)
    print("Projection matrix K2")
    print(K2)


def press(event):
    global ix, iy, fx, fy, drawing
    drawing = True
    ix, iy = event.x, event.y
    # print ("clicked at", event.x, event.y)


def motion(event):
    global ix, iy, fx, fy, drawing
    drawing = True
    fx, fy = event.x, event.y
    # print ("Motion at", event.x, event.y)


def release(event):
    global ix, iy, fx, fy, stdR, stdG, stdB, meanR, meanG, meanB, lower0, upper0, lower1, upper1, thresh1, drawing, lower, upper, ready, setCamID, w
    alfa = int(w.get())
    print(alfa)
    drawing = False
    fx, fy = event.x, event.y
    arrayR = [None] * (ix - fx)
    arrayG = [None] * (ix - fx)
    arrayB = [None] * (ix - fx)
    drawing = False
    # 2 range is used because I do not want to take in account the green line
    for i in range(ix + 2, fx - 2):
        for j in range(iy + 2, fy - 2):
            aux = pic[j, i, [2, 1, 0]]
            arrayR.append(aux[0])
            arrayG.append(aux[1])
            arrayB.append(aux[2])

    meanR, stdR = cv2.meanStdDev(np.array(arrayR))
    meanG, stdG = cv2.meanStdDev(np.array(arrayG))
    meanB, stdB = cv2.meanStdDev(np.array(arrayB))
    lower = np.array(
        [clip(meanR - alfa * stdR), clip(meanG - alfa * stdG), clip(meanB - alfa * stdB)])
    upper = np.array(
        [clip(meanR + alfa * stdR), clip(meanG + alfa * stdG), clip(meanB + alfa * stdB)])

    if setCamID == 0:
        lower0 = lower
        upper0 = upper
    elif setCamID == 1:
        lower1 = lower
        upper1 = upper
    # print ("Release at", ix,iy,fx, fy)
    ready = True


def increase():
    global ax, canvas
    ax.scatter([10, 20, 30], [1, 2, 3], [10, 20, 30], c='r', marker='o')
    canvas.draw()


def plot3DPosition(x1, y1, x2, y2): #plot3D is useful
    global vx, vy, vz, ax, canvas, ax2, canvas2, ax3, canvas3
    # frame = Frame(top)
    # if plotted==False:
    #    frame.configure(bg='white')
    #    frame.grid(column=10, row=0, rowspan=8, padx=10, pady=10)
    #    plotted=True
    # Create the main container
    K1, K2 = readXml()
    h = 1
    Matrix1 = [[x1, y1]]
    Matrix1 = np.array(Matrix1).reshape(2, h)
    # print(Matrix1)
    Matrix2 = [[x2, y2]]
    Matrix2 = np.array(Matrix2).reshape(2, h)
    # print(Matrix2)
    # A*K
    pts4D = cv2.triangulatePoints(K1, K2, Matrix1, Matrix2)
    pts3D = np.transpose(pts4D)
    # transpose(points3DHomogeneous, triangulatedPoints3D)

    # print ("*********pts3D**********")
    pts3D = cv2.convertPointsFromHomogeneous(pts3D)
    # print(pts3D)
    # plot with matplotlib
    Xs = pts3D[:, 0][:, 0]
    Ys = pts3D[:, 0][:, 1]
    Zs = pts3D[:, 0][:, 2]
    # print(Xs)
    # print(Ys)
    # print(Zs)

    # TODO: These scatters are probably doing the plot
    ax.scatter(Xs[0], -Ys[0], Zs[0], c='r', marker='o')
    canvas.draw()

    #ax2.scatter(Xs[0], -Ys[0], c='b', marker='o')
    #canvas2.draw()

    #ax3.scatter(Xs[0], Zs[0], c='b', marker='x')
    #canvas3.draw()

    vx.set('X:' + str(Xs[0]))
    vy.set('Y:' + str(-Ys[0]))
    vz.set('Z:' + str(Zs[0]))


def plotA():
    global ax, canvas, ax2, canvas2, ax3, canvas3, vx, vy, vz
    fig = Figure()
    canvas = FigureCanvasTkAgg(fig, master=top)
    ax = fig.add_subplot(111, projection='3d')
    x = [0]
    y = [0]
    z = [0]

    ax.scatter(x, y, z, c='r', marker='o')
    ax.set_xlabel('X Width')
    ax.set_ylabel('Y Height')
    ax.set_zlabel('Z Depth')
    ax.set_zlim3d((30, 60))
    canvas.draw()
    canvas.get_tk_widget().grid(column=30, row=0, rowspan=10, columnspan=10) #TODO:here is where magic is happening
    """
    fig2 = Figure()
    ax2 = fig2.add_subplot(111)
    ax2.scatter(x, y, c='r', marker='o')
    ax2.set_xlabel('X Width')
    ax2.set_ylabel('Y Height')
    canvas2 = FigureCanvasTkAgg(fig2, master=top)
    canvas2.draw()
    canvas2.get_tk_widget().grid(column=0, row=15, rowspan=10, columnspan=10)
    """
    """
    fig3 = Figure()
    ax3 = fig3.add_subplot(111)
    ax3.scatter(x, z, c='r', marker='o')
    ax3.set_xlabel('X Width')
    ax3.set_ylabel('Z Depth')
    ax3.set_ylim((40, 90))
    canvas3 = FigureCanvasTkAgg(fig3, master=top)
    canvas3.draw()
    canvas3.get_tk_widget().grid(column=10, row=15, rowspan=10, columnspan=10)

    vx.set('X=')
    vy.set('Y=')
    vy.set('Z=')

    wx = Label(top, textvariable=vx, font=("Helvetica", 30))
    wx.grid(column=30, row=10, rowspan=10, columnspan=10)

    wy = Label(top, textvariable=vy, font=("Helvetica", 30))
    wy.grid(column=30, row=15, rowspan=10, columnspan=10)

    wz = Label(top, textvariable=vz, font=("Helvetica", 30))
    wz.grid(column=30, row=20, rowspan=10, columnspan=10)
"""

def show_vid():  # creating a function
    global ix, iy, fx, fy, lmain, lmain2, frame, pic, drawing, lower0, upper0, lower1, upper1, ready, setCamID, closeAll, capAux, cap0, cap1, thresh1, thresh2, trackBall, createPlot
    # time.sleep(0.3)
    if trackBall:
        # lmain2.grid(column=0, row=10, rowspan=8, padx=10, pady=10)
        _, frame = cap0.read()
        frame = cv2.flip(frame, 1)
        thresh1 = cv2.inRange(frame, lower0, upper0)
        x1, y1, frame = followBall(frame, thresh1)

        _, frame2 = cap1.read()
        frame2 = cv2.flip(frame2, 1)
        thresh2 = cv2.inRange(frame2, lower1, upper1)
        x2, y2, frame2 = followBall(frame2, thresh2)

        # we can change the display color of the frame gray,black&white here
        pic = cv2.cvtColor(frame2, cv2.COLOR_BGR2RGB)
        img2 = Image.fromarray(pic)
        imgtk2 = ImageTk.PhotoImage(image=img2)
        lmain2.imgtk = imgtk2
        lmain2.configure(image=imgtk2)
        if not createPlot:
            plotA()
            createPlot = True
        plot3DPosition(x1, y1, x2, y2) #TODO: This must return something to print to the GUI. Using matplotlib embeded in qt

    else:
        if setCamID == 0:
            capAux = cap0
        elif setCamID == 1:
            capAux = cap1
        # if not cap.isOpened():                             #checks for the opening of camera
        #   print("cant open the camera")
        _, frame = capAux.read()
        frame = cv2.flip(frame, 1)
        if ready:
            _, frame = capAux.read()
            frame = cv2.flip(frame, 1)
            if setCamID == 0:
                thresh1 = cv2.inRange(frame, lower0, upper0)
                img2 = Image.fromarray(thresh1)
                _, _, frame = followBall(frame, thresh1)
            if setCamID == 1:
                thresh2 = cv2.inRange(frame, lower1, upper1)
                img2 = Image.fromarray(thresh2)
                _, _, frame = followBall(frame, thresh2)
            imgtk2 = ImageTk.PhotoImage(image=img2)
            lmain2.imgtk = imgtk2
            lmain2.configure(image=imgtk2)
        # lmain2.after(10, show_vid)

    if drawing:
        cv2.rectangle(frame, (ix, iy), (fx, fy), (0, 255, 0), 2)

    # we can change the display color of the frame gray,black&white here
    pic = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    img = Image.fromarray(pic)
    imgtk = ImageTk.PhotoImage(image=img)
    lmain.imgtk = imgtk
    lmain.configure(image=imgtk)
    if not closeAll:
        lmain.after(1, show_vid)
    lmain.bind("<Button-1>", press)
    lmain.bind("<B1-Motion>", motion)
    lmain.bind("<ButtonRelease-1>", release)


def followBall(frame, thresh1):
    kernel1 = np.ones((5, 5), np.uint8)
    mask1 = cv2.morphologyEx(thresh1, cv2.MORPH_OPEN, kernel1)
    kfObj = KalmanFilter()
    predictedCoords = np.zeros((2, 1), np.float32)
    # find contours in the mask1 and initialize the current
    # (x1, y1) center1 of the ball
    cnts1 = cv2.findContours(
        mask1.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    center1 = None
    # only proceed if at least one contour was found
    x1, y1 = 0, 0
    if len(cnts1) > 0:
        # find the largest contour in the mask1, then use
        # it to compute the minimum enclosing circle and
        # centroid
        c1 = max(cnts1, key=cv2.contourArea)
        ((x1, y1), radius1) = cv2.minEnclosingCircle(c1)
        # M1 = cv2.moments(c1)
        # center1 = (int(M1["m10"] / M1["m00"]), int(M1["m01"] / M1["m00"]))
        # only proceed if the radius1 meets a minimum size
        if radius1 > 10:
            # draw the circle and centroid on the frame,
            # then update the list of tracked points
            cv2.circle(frame, (int(x1), int(y1)), int(radius1), (0, 255, 255), 2)
            cv2.line(frame, (int(x1), int(y1 - radius1)), (int(x1 + radius1), int(y1 - radius1)), [0, 255, 255], 2, 8)
            # cv2.circle(frame, center1, 3, (0, 0, 255), -1)
            cv2.putText(frame, "Actual", (int(x1 + radius1), int(y1 - radius1)), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                        [0, 255, 255])
            # cv2.putText(frame, "centroid", (center1[0]+10, center1[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 255), 1)
            # cv2.putText(frame, "("+str(center1[0])+","+str(center1[1])+")",(center1[0]+10, center1[1]+15), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 255), 1)
            predictedCoords = kfObj.Estimate(x1, y1)

            # Draw Kalman Filter Predicted output
            cv2.circle(frame, (predictedCoords[0], predictedCoords[1]), int(radius1), [255, 0, 0], 2, 8)
            cv2.line(frame, (predictedCoords[0] + radius1, predictedCoords[1]),
                     (predictedCoords[0] + 2 * radius1, predictedCoords[1]), [255, 0, 0], 2, 8)
            cv2.putText(frame, "Predicted", (int(predictedCoords[0] + 2 * radius1), int(predictedCoords[1])),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, [255, 0, 0])
    return x1, y1, frame


def clear():
    global closeAll, capAux, cap0, cap1, trackBall, ready
    closeAll = True
    list = top.grid_slaves()
    for l in list:
        l.destroy()
    cap0.release()
    cap1.release()
    # cap0 = cv2.VideoCapture('/home/geova/Documents/SIP-Lab/Kalman/ball.mp4')
    cap0 = cv2.VideoCapture(0)
    cap1 = cv2.VideoCapture(2)  # TODO: change video id here too
    time.sleep(1)
    trackBall = False  # TODO: Changed from == to =
    ready = False

    # ballTracker()


def camera(camID):
    global lmain, lmain2, lmain3, setCamID, closeAll, trackBall, vx, vy, vz, w
    trackBall = False  # TODO: Changed from == to =
    if setCamID == -1 or closeAll == True:
        lmain = Label(master=top)
        #lmain.grid(column=0, row=0, rowspan=4, padx=5, pady=5)
        lmain2 = Label(master=top)
        #lmain2.grid(column=10, row=0, rowspan=8, padx=10, pady=10)
        vx = StringVar()
        vy = StringVar()
        vz = StringVar()
        barValue = StringVar()
        # lmain3 = Label(master=top, textvariable=v)
        # lmain3.grid(column=10, row=10, rowspan=4, padx=4, pady=4)
        # v.set("3D Reconstruction X,Y,Z")
        setCamID = camID
        lmain4 = Label(master=top, text="Factor de  precisión")
        #lmain4.grid(column=0, row=10, rowspan=4, padx=4, pady=4)
        #w = Scale(top, from_=0, to=2, resolution=0.5, orient=HORIZONTAL)
        #w.set(2)
        #w.grid(column=0, row=15, rowspan=8, padx=10, pady=10)
        # print(w.get())
        closeAll = False
        show_vid()
    setCamID = camID


def ballTracker():
    global trackBall
    trackBall = True


def main(args):
    return 0


def callback(value):
    pass


def openVideo(value):
    lmain = Label(master=top)
   # lmain.grid(column=0, row=0, rowspan=4, padx=5, pady=5)


if __name__ == '__main__':
    top = Tk()

    top.title("Triangulation Project")  # you can give any title
    top.attributes("-zoomed", True)  # This sets if the windows starts in fullscreen or not
    menubar = Menu(top)
    file = Menu(menubar, tearoff=0)  #
    file.add_command(label="SeeProjectionMatrix", command=showProjectionMatrix)
    file.add_separator()
    file.add_command(label="CalibrateDemoCamera1", command=partial(camera, 0))
    file.add_command(label="CalibrateDemoCamera2", command=partial(camera, 1))
    file.add_command(label="TrackBall", command=partial(ballTracker))
    file.add_command(label="OpenVideo", command=partial(openVideo))
    file.add_command(label="ClearScreen", command=partial(clear))
    file.add_separator()
    file.add_command(label="Exit", command=top.quit)
    menubar.add_cascade(label="File", menu=file)
    top.config(menu=menubar)
    top.mainloop()

# cap.release()
