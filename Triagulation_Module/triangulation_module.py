from tkinter import *
import tkinter as tk
import sys

from PyQt5 import QtCore
from Triagulation_Module.readXML import readXml
import numpy as np
import cv2
import random
from PIL import Image, ImageTk
import os
from functools import partial
import math
import time
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from mpl_toolkits.mplot3d import Axes3D
K1, K2 = np.zeros((3, 4), np.float32), np.zeros((3, 4), np.float32)



def showProjectionMatrix():
    K1, K2 = readXml()
    print("Projection matrix K1")
    print(K1)
    print("Projection matrix K2")
    print(K2)



def plot3DPosition(x1, y1, x2, y2, figure,path): #plot3D is useful
    global vx, vy, vz, ax, canvas, ax2, canvas2, ax3, canvas3
    K1, K2 = readXml(path)
    h = 1
    Matrix1 = [[x1, y1]]
    Matrix1 = np.array(Matrix1).reshape(2, h)
    # print(Matrix1)
    Matrix2 = [[x2, y2]]
    Matrix2 = np.array(Matrix2).reshape(2, h)
    # print(Matrix2)
    pts4D = cv2.triangulatePoints(K1, K2, Matrix1, Matrix2) #Finds the points
    pts3D = np.transpose(pts4D)

    pts3D = cv2.convertPointsFromHomogeneous(pts3D)
    # print(pts3D)
    # plot with matplotlib
    Xs = pts3D[:, 0][:, 0]
    Ys = pts3D[:, 0][:, 1]
    Zs = pts3D[:, 0][:, 2]
    print("Values:")
    print(Xs)
    print(Ys)
    print(Zs)
    print("+++++++++++")
    #fig = Figure()
    #plt_fig = plt.figure()
    #canvas = FigureCanvasTkAgg(fig, master=top)
    #ax = fig.add_subplot(111, projection='3d')
    #ax = plt_fig.add_subplot(111, projection='3d')
    #x = [0]
    #y = [0]
    #z = [0]
    #ax.scatter(x, y, z, c='r', marker='o')
    # TODO: These scatters do the plot
    #ax.scatter(Xs[0], -Ys[0], Zs[0], c='r', marker='o')
    figure.scatter(Xs[0], -Ys[0], Zs[0], c='r', marker='o')

    #canvas.draw()
    #ax.canvas.draw()

    #ax2.scatter(Xs[0], -Ys[0], c='b', marker='o')
    #canvas2.draw()

    #ax3.scatter(Xs[0], Zs[0], c='b', marker='x')
    #canvas3.draw()

    #vx.set('X:' + str(Xs[0]))
    #vy.set('Y:' + str(-Ys[0]))
    #vz.set('Z:' + str(Zs[0]))

    return [Xs,Ys,Zs]

def main():
    a = []
    b = []
    plt_fig = plt.figure()
    #canvas = FigureCanvasTkAgg(fig, master=top)
    #ax = fig.add_subplot(111, projection='3d')
    ax = plt_fig.add_subplot(111, projection='3d')
    test = open("/home/disoji/Documents/Gits/GIT_Proyecto_DISEÑO/integrated_butterfly_tracking_tool/Triagulation_Module/triangulationTest.txt",'r')
    temp = test.readline()
    while temp != '':
        temp.replace("\n","")
        temp = temp.split(' ')
        temp = plot3DPosition(float(temp[0]), float(temp[1]), float(temp[0]), float(temp[2]), ax)
        print("temp")
        print(temp)
        print("------")
        temp = test.readline()
    plt.show()
"""
    for i in range(0,100):
        ran_num = random.randrange(100)
        print("ran_num")
        print(ran_num)
        a = a + [ran_num]
        b = b + [ran_num+1]
    for i in range(0,100):
        temp = plot3DPosition(a[i],b[i],b[i],b[i],ax)
        print(temp)
        print("------")
    """

"""
class MyDynamicMplCanvas(MyMplCanvas):
    "A canvas that updates itself every second with a new plot.

    def __init__(self, *args, **kwargs):
        MyMplCanvas.__init__(self, *args, **kwargs)
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update_figure)
        timer.start(1000)

    def compute_initial_figure(self):
        self.axes.plot_wireframe([0, 1, 2, 3], [1, 2, 0, 4], [1, 2, 0, 4])

    def update_figure(self):
        # Build a list of 4 random integers between 0 and 10 (both inclusive)
        #l = [random.randint(0, 10) for i in range(1)]
        #self.axes.cla()
        self.axes.plot_wireframe([0, 1, 2], l, l, color='r')
        self.draw()
"""


if __name__ == '__main__':
    main()