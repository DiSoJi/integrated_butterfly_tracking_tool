import PySpin
import datetime
import os
from CalibrationMain_Module.src.utils.ringBuffer import RingBuffer
from time import sleep


class flir_camera():  # This class is basically the PySpin.CameraPtr class with more methods and attributes. #TODO inherit the PySpin.CameraPtr class to this class
    def __init__(self, serial, is_master):
        self.cam = 0
        self.serial = serial  # String of the camera's serial number. Ex. '18281928'
        self.is_master = is_master  # Boolean value True or False, that denotes if the camera is master or slave

    def init_camera(self, camList,
                    fps):  # camList is the list of cameras of the PySpin module, of type = PySpin.CameraList and can be get with "PySpin.System.GetInstance().GetCameras()"
        self.cam = camList.GetBySerial(self.serial)
        """cam_master = flir_camera('18285942', True) # Master
        cam_slave_1 = flir_camera('18286263', False) # Slave
		cam_slave_2 = flir_camera('18286257', False) # Slave"""
        # Initialize camera
        if self.serial == '18286257':  # TODO:FIXED patch
            print("third camera is bothering")
        else:
            self.cam.Init()

        # For the master camera
        if self.is_master:
            self.cam.LineSelector.SetValue(PySpin.LineSelector_Line2)
            self.cam.V3_3Enable.SetValue(True)
        # For the slave camera
        else:
            if self.serial != '18286257':  # TODO:FIXED patch
                self.cam.TriggerMode.SetValue(PySpin.TriggerMode_Off)
                self.cam.TriggerSource.SetValue(PySpin.TriggerSource_Line3)
                self.cam.TriggerOverlap.SetValue(PySpin.TriggerOverlap_ReadOut)
                self.cam.TriggerMode.SetValue(PySpin.TriggerMode_On)
        # Set continuous acquisition mode
        if self.serial != '18286257':  # TODO:FIXED patch
            self.cam.AcquisitionMode.SetValue(PySpin.AcquisitionMode_Continuous)  # SingleFrame for just one frame
            self.cam.AcquisitionFrameRateEnable.SetValue(True)
            self.cam.AcquisitionFrameRate.SetValue(fps)
        print("Everything ok in init_camera")

    def start_acquisition(self):
        print("Starting acquisition of ", self.serial, " camera")
        self.cam.BeginAcquisition()


class flir_controller():  # set of N FLIR cameras
    # Init PySpin System
    def __init__(self, interval, fps):
        self.system = PySpin.System.GetInstance()
        self.interval = interval
        self.fps = fps

    # Get Cameras
    def get_cameras(self, cameras):
        self.cameras = cameras

    # Initialize cameras
    def init_cameras(self):
        self.camList = self.system.GetCameras()
        for camera in self.cameras:
            camera.init_camera(self.camList, self.fps)

    def start_acquisition(self):
        for camera in self.cameras:
            if camera.is_master:
                master_camera = camera  # save master camera to initialize at the end
            else:
                camera.start_acquisition()
        # Start acquisition of master camera when slave have begun
        master_camera.start_acquisition()

    def end_acquisition(self):
        for camera in self.cameras:
            camera.cam.EndAcquisition()

    def save_images(self, ringBuffer, save_mem_flag, time_recording, amount_frames, stop_recording, calName):
        time = datetime.datetime.now()
        # TODO: Set correct folder according to need - COMPLETED: takes calibration name and appends rest of folder
        img_folder = "./exp_files/calibraciones/" + str(calName[0]) + "/medios/fotos/" + str(time) + "/"
        os.mkdir(img_folder)
        print('Data will be saved into: ', img_folder)

        frames = round((amount_frames / 3) * time_recording)
        for i in range(frames):
            while not stop_recording[0]:
                images = []
                for camera in self.cameras:
                    images.append(camera.cam.GetNextImage())

                for j, image in enumerate(images):  # Save images
                    frameNumber = str(i).zfill(4)
                    camNumber = str(j)
                    if save_mem_flag:
                        image.Save(img_folder + camNumber + '-' + frameNumber + '.png')  # TODO:Path changed here
                    # TODO: Add here ring buffer tinkering - Buffer working. But FLIR Capture is not as precise as expected
                    ringBuffer.append(image)
                    # Release images
                    image.Release()
        return 0

    def end_system(self):

        for camera in self.cameras:
            del camera.cam
            del camera

        self.camList.Clear()
