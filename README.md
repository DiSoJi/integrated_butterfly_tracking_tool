First run dependencies.sh using sh

Then you need Spinnaker:

Spinnaker 1.19 for Python (PySpin)
FLIR cameras ar eused. The Blackfly S BFS-U3-13Y3M-C. Get the software in FLIR | Machine vision -> File Downloads. This is in case of using the industrial cameras
It is necessary to register to get the python distribution for your python version and architecture, also remember that thte python version is nothing but a wrapper you still need the standard C version (in the site where you download the wheel file for linux there should be a folder called python and other files, those files are the standard version and in the folder called python there is the wrapper). The folder you download as a compressed file contains a README for installation.

Increase USB file system memory. In /etc/default/grub change (open it with vim or nano as sudo)

GRUB_CMDLINE_LINUX_DEFAULT="quiet splash" (It may have other arguments, just write a whitespace after the last argument and add the following change)

To

GRUB_CMDLINE_LINUX_DEFAULT="quiet splash usbcore.usbfs_memory_mb=1000"

Then run:

$ sudo update-grub2 (or installed grub version)

Update grub with these settings:

$ sudo update-grub

Reboot and test a USB 3.1 camera.

If this method fails to set the memory limit, run the following command:

$ sudo sh -c 'echo 1000 > /sys/module/usbcore/parameters/usbfs_memory_mb'

To confirm that you have successfully updated the memory limit, run the following command:
$ cat /sys/module/usbcore/parameters/usbfs_memory_mb
